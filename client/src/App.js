import './App.css';
import Navbar from './components/Nav/Nav';
import Home from './components/Home/Home';
import VideoPlayer from './components/VideoPlayer/VideoPlayer';
import History from './components/History/History';

import { BrowserRouter, Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />

        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/watch" component={VideoPlayer}/>
          <Route path="/history" component={History}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
