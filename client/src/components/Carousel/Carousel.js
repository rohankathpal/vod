import React, { Component } from 'react';
import ImagePane from './ImagePane';
import { withRouter } from 'react-router-dom';
import right from '../../icons/chevron-right.svg';
import left from '../../icons/chevron-left.svg';
import config from  '../../environment';

class Carousel extends Component {

    //to access the carousel dom element
    carouselRef = React.createRef();

    componentDidMount() {
        window.addEventListener("keydown", this.keyboardSupport);
    };


    /* handler for right click button*/
    handleLeftClick = () => {
        if (this.carouselRef.current.scrollLeft < 450) {
            this.carouselRef.current.scroll(0, 0);
        } else {
            this.carouselRef.current.scroll(this.carouselRef.current.scrollLeft - 450, 0);
        }
    };

    /* handler for right click button*/
    handleRightClick = () => {
        if (this.carouselRef.current.scrollLeft + 450 > this.carouselRef.current.scrollWidth) {
            this.carouselRef.current.scroll(this.carouselRef.current.scrollWidth, 0);
        } else {
            this.carouselRef.current.scroll(this.carouselRef.current.scrollLeft + 450, 0);
        }

    }

    /* updates viewer's history in database */
    updateUserWatchHistoryInfo = async (params) => {
        const payload = {
            ...params,
            deviceId: localStorage.deviceId,
        };
        try {
            const url = '/api/session/updateInfo';
            const response = await fetch(`${config.baseUrl}${url}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payload)
            });
            return await response.json();
        } catch (err) {
            console.log(err);
        }
    }

    /* keyboard support for carousel */
    keyboardSupport = (e) => {
        const { key } = e;
        if (key === "ArrowLeft") {
            this.handleLeftClick();
        } else if (key === "ArrowRight") {
            this.handleRightClick();
        }
    }

    /* wrapper function for updateUserWatchHistoryInfo */
    onPlay = async (params) => {
        await this.updateUserWatchHistoryInfo(params);
    }

    render() {
        const { movieData } = this.props;
        return (<div className="carouselBase" ref={this.carouselRef} onKeyPress={this.keyboardSupport}>
            <button onClick={this.handleLeftClick} className="btn btn-left">
                <img src={left} className="btn-icon" alt="img"/>
            </button>
            {
                movieData.entries.map((data) => {
                    return (<ImagePane imgData={data} onPlay={this.onPlay} key={data.id}/>)
                })
            }
            <button onClick={this.handleRightClick} className="btn btn-right">
                <img src={right} className="btn-icon" alt="img"/>
            </button>
        </div>);
    }
}

export default withRouter(Carousel);