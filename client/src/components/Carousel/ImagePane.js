import { Link } from 'react-router-dom';
import './Carousel.css';

function ImagePane({ imgData, onPlay }) {
    return (
        <div className="imagePane" onClick={() => onPlay({
            videoURL: imgData.contents[0].url,
            title: imgData.title,
            posterURL: imgData.images[0].url,
            entryId: imgData.id
        })}
        >
            <Link to={{
                pathname : "/watch",
                query : {
                    url: imgData.contents[0].url,
                    poster: imgData.images[0].url,
                }
            }}>
                <img className="image" src={imgData.images[0].url} alt="alt text" />
                <h4 className="description">
                    {imgData.title}
                </h4>
            </Link>
        </div>
    );
};

export default ImagePane
