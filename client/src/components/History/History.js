import { Component } from "react";
import Carousel from "../Carousel/Carousel";
import './History.css';
import config from '../../environment';
import Loader from "../Loader/Loader";

class History extends Component {

    state = {
        movieData: {
            entries: []
        },
        deviceId: null,
        loading : true
    };

    componentDidMount() {
        /* Getting deviceId from localstorage */
        let deviceId = localStorage.getItem('deviceId');

        /* Getting viewer's history data based on deviceId */
        this.getHistoryData(deviceId)
            .then((data) => {
                this.setState({
                    movieData: {
                        entries: this.convertHistoryDataToMovieData(data),
                    },
                    loading : false
                });
            })
            .catch((err) => {
                console.log(err);
            });
    }

    /* Transforming data to the Carousel's acceptable structure */
    convertHistoryDataToMovieData(data) {
        return data.map((datum) => ({
            title: datum.title,
            contents: [{
                url: datum.videoURL
            }],
            images: [{
                url: datum.posterURL
            }],
            id: datum.entryId
        }))
    }

    getHistoryData = async (deviceId) => {
        try {
            const resp = await fetch(`${config.baseUrl}/api/session/getHistory?deviceId=${deviceId}`);
            return await resp.json();
        } catch (err) {
            console.log(err);
        }
    };

    render() {
        const { movieData, loading } = this.state;
        return (
            <div className="frag">
                {
                    loading ? <Loader/> : 
                    (
                        movieData.entries.length > 0 ?
                        (
                            <>
                                <h3>Recently watched</h3>
                                <Carousel movieData={movieData} />
                            </>
                        )
                        :
                        <div className="empty"><h3>No Device History</h3></div>
                    )
                }
            </div>
        )
    }
}

export default History;