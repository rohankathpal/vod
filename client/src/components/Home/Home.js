import { Component } from "react";
import Carousel from "../Carousel/Carousel";
import Loader from "../Loader/Loader";
import { v4 as uuidv4, validate as uuidValidate } from 'uuid';
import './Home.css';
import config from '../../environment';

class Home extends Component {

    state = {
        movieData: {
            entries: []
        },
        trending: {
            entries: []
        },
        deviceId: null,
        loading: true
    };

    async componentDidMount() {
        /* Adding deviceId to localstorage to keep track of user sessions */

        let deviceId = localStorage.getItem('deviceId')
        if (!uuidValidate(deviceId)) {
            deviceId = uuidv4();
            localStorage.setItem('deviceId', deviceId);
        }

        /* Getting data from Accedo API and TMDB API */
        let response = await Promise.all([fetch(`${config.baseUrl}/api/external/getMovieData`), fetch(`${config.baseUrl}/api/external/getTrending`)]);
        response = await Promise.all(response.map((data) => data.json()));
        this.setState({
            movieData: response[0],
            trending: response[1],
            loading : false
        })
    }

    render() {
        const { movieData, trending, loading } = this.state;
        return (
            <div className="frag">
                {
                    loading == true  ? (
                        <Loader/>
                    ) : (
                        <>
                        <h3>Watch Next</h3>
                        <Carousel movieData={movieData} />
                        <h3>Trending </h3>
                        <Carousel movieData={trending} />
                        </>
                    )
                }
            </div>
        )
    }
}

export default Home;