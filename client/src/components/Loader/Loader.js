import { Component } from "react";

import './Loader.css';

function Loader(){
    return (
        <h2 className="load">Loading . . .</h2>
    )
}
export default Loader
