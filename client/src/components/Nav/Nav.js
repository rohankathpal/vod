import React, {Component} from 'react';
import "./Nav.css";
import { Link } from 'react-router-dom';
class Navbar extends Component{
    render(){
        return(
            <div className="nav-parent">
                <ul className="nav-base">
                    <li className="nav-elem home">
                        <Link to="/">
                                Home
                        </Link>
                    </li>
                    <li className="nav-elem history">
                        <Link to="/history">
                                History
                        </Link>
                    </li>
                </ul>
            </div>
        )
    }
}

export default Navbar