import React, { Component } from "react";
import './VideoPlayer.css';
import playicon from '../../icons/play-button-arrowhead.svg'
import { withRouter } from 'react-router-dom';


class VideoPlayer extends Component { 
    
    state = {
        isPlaying : false,
        video : {
            url : "",
            poster : ""
        }
    };

    videoRef = React.createRef();

    componentDidMount(){
        const { location } = this.props;
        if(location.query){
            localStorage.videoData = JSON.stringify(location.query);
            this.setVideoInfo(location.query);
        }else if(localStorage.videoData){
            this.setVideoInfo(JSON.parse(localStorage.videoData));
        }
    }
    
    setVideoInfo = (params)=>{
        this.setState({
            video : params
        });
    };

    goHome = () => {
        this.props.history.push('/');
    }

    play = () => {
        this.setState({
            isPlaying: true
        });
    };

    pause = () => {
        this.setState({
            isPlaying: false
        });
    };

    playtoggle = ()=>{
        this.setState({
            isPlaying : !this.state.isPlaying
        });
        this.videoRef.current.play()
    }

    render (){
        const { video , isPlaying } = this.state;
        return (
            <div className="video-container">
                { !isPlaying && <div className="play-icon-div">
                    <img src={playicon} className="play-icon" alt="icon" onClick={this.playtoggle}/>
                </div>
                }
                <video ref={this.videoRef} onEnded={this.goHome} onPause={this.pause} onPlay={this.play} onClick={this.playNow} className="v-player" controls poster={video.poster}>
                    <source src={video.url} type="video/mp4" />
                </video>
            </div>
        )
    }
}

export default withRouter(VideoPlayer);