VOD is a small project developed with love in React and Node and hosted on AWS ECS.
The content is powered by The Movie Database (TMDB) API.

Features : 
	Browser backed sessions
	Fullscreen Video Playback
	User's Viewing History
	Latest Trending Movies / TV shows

Prerequisites : 
	Docker

To run locally : 
    sudo service docker start  // to start the docker service 
	docker run -p 3000:3000 rohan1992/voclient:latest