const request = require('request');
const { API_KEY, BaseImgUrl } = require('../../config');
const getMovieData = (req,res)=>{
    const options = {
        method : "GET",
        url : "https://accedo-ps-programming-exam.s3-ap-southeast-1.amazonaws.com/movies.json"
    };
    request(options,(error,response)=>{
        if(error){
            return res.status(500).json(error);
        }
        return res.status(response.statusCode).json(JSON.parse(response.body));
    });
};

const getTrending = (req,res)=>{
    const options = {
        method : "GET",
        url : `https://api.themoviedb.org/3/trending/all/day?api_key=${API_KEY}`
    };
    request(options,(error,response)=>{
        if(error){
            return res.status(500).json(error);
        }
        let entries = convertDataToMovieDataStructure(JSON.parse(response.body).results)
        return res.status(response.statusCode).json({ entries, totalCount:entries.length });
    });
};

const convertDataToMovieDataStructure = (data)=>{
    return data.map((datum)=>({
        title : datum.title || datum.name,
        description : datum.overview || "",
        contents : [{
            url : "http://d2bqeap5aduv6p.cloudfront.net/project_coderush_640x360_521kbs_56min.mp4"
        }],
        images: [{
            url : `${BaseImgUrl}${datum.poster_path}`
        }],
        id : datum.id
    }));
};


module.exports = {
    getMovieData,
    getTrending
};