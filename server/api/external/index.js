const aceedo = require('./external');

const router = require('express').Router();
const external = require('./external');

router.get('/getMovieData',external.getMovieData);
router.get('/getTrending',external.getTrending);

module.exports = router;

