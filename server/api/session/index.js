const router = require('express').Router();
const session = require('./session');

router.post('/updateInfo', session.updateInfo);
router.get('/getHistory', session.getHistory);

module.exports = router;

