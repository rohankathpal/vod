const session = require('./session.model');
const { handleError } = require('../../services/utility/utils');

const updateInfo = (req, res) => {
    try {
        let { body: payload } = req;
        let { deviceId, entryId } = payload;
        payload = {
            timestamp: new Date,
            ...payload
        };  

        const requiredKeys = ["timestamp","videoURL","title","posterURL","entryId","deviceId"];
        const missingKeys = requiredKeys.filter((key)=>!payload.hasOwnProperty(key));
        if(missingKeys.length){
            return handleError(res,{ missingKeys , message : "Invalid Payload" },422);
        }

        session.findOneAndUpdate({ deviceId, entryId }, payload, { upsert: true,runValidators: true }, (err, result) => {
            if (err) {
                return handleError(res, err);
            } else {
                return res.status(200).json({ "message": "updated" });
            }
        })
    } catch (err) {
        return handleError(res, err);
    }
};

const getHistory = (req, res) => {
    try {
        const { deviceId } = req.query;

        if(!deviceId){
            return handleError(res,{ missingKeys : ["deviceId"], message : "Invalid Payload" },422);
        }

        session
            .find({ deviceId })
            .sort({ timestamp: -1 })
            .limit(30)
            .lean()
            .exec((err, result) => {
                if (err) {
                    return handleError(res, err);
                }
                return res.status(200).json(result);
            })
    } catch (err) {
        return handleError(res, err);
    }
}

module.exports = {
    updateInfo,
    getHistory
};