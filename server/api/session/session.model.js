const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SessionSchema = new Schema({
    timestamp: {
        type: Date,
        default: Date.now
    },
    deviceId: {
        type: String,
        "required": true,
    },
    videoURL: {
        type: String,
        "required": true,
    },
    title: {
        type: String,
        required: true,
    },
    posterURL: {
        type: String,
        required: true,
    },
    entryId: {
        type: String,
        required: true,
    }
});



module.exports = mongoose.model('Session', SessionSchema);
