const express = require('express');
const app = express();
const port = 3001;
const cors = require('cors');

app.use(cors());

app.use(express.json());

require('./route')(app);

require('mongoose').connect('mongodb+srv://root:root@cluster0.yebyt.mongodb.net/VOD', {useNewUrlParser: true, useUnifiedTopology: true});

app.listen(port,()=>console.log(`Express server is listening at port ${port}`));

module.exports = app;
