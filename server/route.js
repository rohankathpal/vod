
module.exports = (app)=>{
    app.use('/api/external',require('./api/external/'));
    app.use('/api/session',require('./api/session/'));
};