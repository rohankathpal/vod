const handleError = (res,error,status=500)=>{
    error = error.stack ? { message : error.message, stack : error.stack } : error; 
    res.status(status).json({error});
}

module.exports = {
    handleError
};