const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../app');


describe('Testing external APIs', () => {
    it('getMovieData API', (done) => {
        request(app).get('/api/external/getMovieData')
            .expect(200)
            .then((res) => {
                const { body } = res;
                expect(body).to.contain.property('totalCount');
                expect(body.totalCount).to.be.equal(30);
                expect(body.entries).to.be.an('array');
                done();
            })
            .catch((err) => done(err));
    });
    it('getTrending API', (done) => {
        request(app).get('/api/external/getMovieData')
            .expect(200)
            .then((res) => {
                const { body } = res;
                expect(body).to.contain.property('totalCount');
                expect(body.entries).to.be.an('array');
                done();
            })
            .catch((err) => done(err));
    });
});

describe('Testing session APIs', () => {
    it('updateInfo endpoint with valid payload', (done) => {
        request(app).post('/api/session/updateInfo')
            .expect(200)
            .send({
                "videoURL": "http://d2bqeap5aduv6p.cloudfront.net/project_coderush_640x360_521kbs_56min.mp4",
                "title": "10 Things I Hate About You",
                "posterURL": "http://lorempixel.com/214/317/?t=1",
                "entryId": "10-things-i-hate-about-you",
                "deviceId": "3ae7d2b3-8881-417f-aaa6-15d723015ab7"
            })
            .then((res) => {
                const { body } = res;
                expect(body).to.contain.property("message");
                expect(body.message).to.be.equal("updated");
                done();
            })
            .catch((err) => done(err));
    });
    it('updateInfo endpoint with invalid payload', (done) => {
        request(app).post('/api/session/updateInfo')
            .send({
                "videoURL": "http://d2bqeap5aduv6p.cloudfront.net/project_coderush_640x360_521kbs_56min.mp4",
                "posterURL": "http://lorempixel.com/214/317/?t=1",
                "deviceId": "3ae7d2b3-8881-417f-aaa6-15d723015ab7"
            })
            .expect(422)
            .then((res) => {
                const { body } = res;
                const { error } = body;
                expect(error).to.contain.property('missingKeys');
                expect(error.missingKeys[0]).to.be.equal("title");
                expect(error.missingKeys[1]).to.be.equal("entryId");
                done();
            })
            .catch((err) => {
                console.log(err);
                done();
            });
    });
    it('getHistory endpoint with deviceId', (done) => {
        request(app).get('/api/session/getHistory?deviceId="3ae7d2b3-8881-417f-aaa6-15d723015ab7"')
            .expect(200)
            .then((res) => {
                const { body } = res;
                expect(body).to.be.an('array');
                done();
            })
    })
    it('getHistory endpoint without deviceId', (done) => {
        request(app).get('/api/session/getHistory')
            .expect(422)
            .then((res) => {
                const { body } = res;
                const { error } = body;
                expect(error).to.contain.property('missingKeys');
                expect(error.missingKeys[0]).to.be.equal("deviceId");
                done();
            })
    })
});